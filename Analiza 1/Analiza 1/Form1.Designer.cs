﻿namespace Analiza_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sin = new System.Windows.Forms.Button();
            this.cos = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.mnozenje = new System.Windows.Forms.Button();
            this.djeljenje = new System.Windows.Forms.Button();
            this.logaritam = new System.Windows.Forms.Button();
            this.korijen = new System.Windows.Forms.Button();
            this.kvadrat = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.op1 = new System.Windows.Forms.TextBox();
            this.op2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rez = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // sin
            // 
            this.sin.Location = new System.Drawing.Point(53, 114);
            this.sin.Name = "sin";
            this.sin.Size = new System.Drawing.Size(85, 37);
            this.sin.TabIndex = 1;
            this.sin.Text = "sin";
            this.sin.UseVisualStyleBackColor = true;
            this.sin.Click += new System.EventHandler(this.sin_Click);
            // 
            // cos
            // 
            this.cos.Location = new System.Drawing.Point(175, 114);
            this.cos.Name = "cos";
            this.cos.Size = new System.Drawing.Size(86, 36);
            this.cos.TabIndex = 2;
            this.cos.Text = "cos";
            this.cos.UseVisualStyleBackColor = true;
            this.cos.Click += new System.EventHandler(this.cos_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(54, 163);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(83, 37);
            this.plus.TabIndex = 3;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(175, 166);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(85, 33);
            this.minus.TabIndex = 4;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // mnozenje
            // 
            this.mnozenje.Location = new System.Drawing.Point(287, 115);
            this.mnozenje.Name = "mnozenje";
            this.mnozenje.Size = new System.Drawing.Size(86, 34);
            this.mnozenje.TabIndex = 5;
            this.mnozenje.Text = "*";
            this.mnozenje.UseVisualStyleBackColor = true;
            this.mnozenje.Click += new System.EventHandler(this.mnozenje_Click);
            // 
            // djeljenje
            // 
            this.djeljenje.Location = new System.Drawing.Point(287, 166);
            this.djeljenje.Name = "djeljenje";
            this.djeljenje.Size = new System.Drawing.Size(85, 33);
            this.djeljenje.TabIndex = 6;
            this.djeljenje.Text = "/";
            this.djeljenje.UseVisualStyleBackColor = true;
            this.djeljenje.Click += new System.EventHandler(this.djeljenje_Click);
            // 
            // logaritam
            // 
            this.logaritam.Location = new System.Drawing.Point(56, 214);
            this.logaritam.Name = "logaritam";
            this.logaritam.Size = new System.Drawing.Size(81, 33);
            this.logaritam.TabIndex = 7;
            this.logaritam.Text = "log";
            this.logaritam.UseVisualStyleBackColor = true;
            this.logaritam.Click += new System.EventHandler(this.logaritam_Click);
            // 
            // korijen
            // 
            this.korijen.Location = new System.Drawing.Point(176, 215);
            this.korijen.Name = "korijen";
            this.korijen.Size = new System.Drawing.Size(83, 31);
            this.korijen.TabIndex = 8;
            this.korijen.Text = "sqrt";
            this.korijen.UseVisualStyleBackColor = true;
            this.korijen.Click += new System.EventHandler(this.korijen_Click);
            // 
            // kvadrat
            // 
            this.kvadrat.Location = new System.Drawing.Point(287, 214);
            this.kvadrat.Name = "kvadrat";
            this.kvadrat.Size = new System.Drawing.Size(84, 32);
            this.kvadrat.TabIndex = 9;
            this.kvadrat.Text = "sqr";
            this.kvadrat.UseVisualStyleBackColor = true;
            this.kvadrat.Click += new System.EventHandler(this.kvadrat_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "prvi operand";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "drugi operand";
            // 
            // op1
            // 
            this.op1.Location = new System.Drawing.Point(170, 28);
            this.op1.Name = "op1";
            this.op1.Size = new System.Drawing.Size(90, 22);
            this.op1.TabIndex = 12;
            this.op1.TextChanged += new System.EventHandler(this.op1_TextChanged);
            // 
            // op2
            // 
            this.op2.Location = new System.Drawing.Point(170, 67);
            this.op2.Name = "op2";
            this.op2.Size = new System.Drawing.Size(90, 22);
            this.op2.TabIndex = 13;
            this.op2.TextChanged += new System.EventHandler(this.op2_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 270);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "rezultat";
            // 
            // rez
            // 
            this.rez.Location = new System.Drawing.Point(120, 267);
            this.rez.Name = "rez";
            this.rez.Size = new System.Drawing.Size(100, 22);
            this.rez.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 376);
            this.Controls.Add(this.rez);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.op2);
            this.Controls.Add(this.op1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.kvadrat);
            this.Controls.Add(this.korijen);
            this.Controls.Add(this.logaritam);
            this.Controls.Add(this.djeljenje);
            this.Controls.Add(this.mnozenje);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.cos);
            this.Controls.Add(this.sin);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button sin;
        private System.Windows.Forms.Button cos;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button mnozenje;
        private System.Windows.Forms.Button djeljenje;
        private System.Windows.Forms.Button logaritam;
        private System.Windows.Forms.Button korijen;
        private System.Windows.Forms.Button kvadrat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox op1;
        private System.Windows.Forms.TextBox op2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox rez;
    }
}

