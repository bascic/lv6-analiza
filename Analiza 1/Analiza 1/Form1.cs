﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Analiza_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        double a;
        double b;


        private void plus_Click(object sender, EventArgs e)
        {
            rez.Text = (a + b).ToString();

        }

        private void op1_TextChanged(object sender, EventArgs e)
        {
            if (double.TryParse(op1.Text, out a))
            {
                double.TryParse(op1.Text, out a);
            }
            else
            {
                op1.Text = "";
                MessageBox.Show("Unesite realni broj!", "Greška!");
            }
        }

        private void op2_TextChanged(object sender, EventArgs e)
        {
            if (double.TryParse(op2.Text, out b))
            {
                double.TryParse(op2.Text, out b);
            }
            else
            {
                op2.Text = "";
                MessageBox.Show("Unesite realni broj!", "Greška!");
            }
        }

        private void minus_Click(object sender, EventArgs e)
        {
            rez.Text = (a - b).ToString();
        }

        private void djeljenje_Click(object sender, EventArgs e)
        {
            rez.Text = (a / b).ToString();
        }

        private void mnozenje_Click(object sender, EventArgs e)
        {
            rez.Text = (a * b).ToString();
        }

        private void logaritam_Click(object sender, EventArgs e)
        {
            rez.Text =Math.Log(a).ToString();
        }

        private void korijen_Click(object sender, EventArgs e)
        {
            rez.Text = Math.Sqrt(a).ToString();
        }

        private void kvadrat_Click(object sender, EventArgs e)
        {
            rez.Text = (a*a).ToString();
        }

        private void sin_Click(object sender, EventArgs e)
        {
            rez.Text = Math.Sin(a).ToString();
        }

        private void cos_Click(object sender, EventArgs e)
        {
            rez.Text = Math.Cos(a).ToString();
        }
    }
}
