﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Analiza2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        string rijec;
        int zivot = 10;
        int pogodak = 0;
        Random rnd = new Random();
        List<string> rijeci = new List<string>();
        string path = "C:\\Users\\1999b\\source\\repos\\Analiza2\\Analiza2\\rijeci.txt";
        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    rijeci.Add(line);
                }
            }

            int r = rnd.Next(rijeci.Count);
            rijec = (string)rijeci[r];

            if(rijec.Length == 1)
            {
                label2.Text = "_";
                label3.Text = "";
                label4.Text = "";
                label5.Text = "";
                label6.Text = "";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
                label10.Text = "";
                label11.Text = "";
            }
            else if(rijec.Length == 2)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "";
                label5.Text = "";
                label6.Text = "";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
                label10.Text = "";
                label11.Text = "";
            }
            else if (rijec.Length == 3)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "";
                label6.Text = "";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
                label10.Text = "";
                label11.Text = "";
            }
            else if (rijec.Length == 4)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
                label10.Text = "";
                label11.Text = "";

            }
            else if (rijec.Length == 5)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
                label10.Text = "";
                label11.Text = "";
            }
            else if (rijec.Length == 6)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "_";
                label8.Text = "";
                label9.Text = "";
                label10.Text = "";
                label11.Text = "";
            }
            else if (rijec.Length == 7)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "_";
                label8.Text = "_";
                label9.Text = "";
                label10.Text = "";
                label11.Text = "";

            }
            else if (rijec.Length == 8)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "_";
                label8.Text = "_";
                label9.Text = "_";
                label10.Text = "";
                label11.Text = "";

            }
            else if (rijec.Length == 9)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "_";
                label8.Text = "_";
                label9.Text = "_";
                label10.Text = "_";
                label11.Text = "";

            }
            else if (rijec.Length == 10)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "_";
                label8.Text = "_";
                label9.Text = "_";
                label10.Text = "_";
                label11.Text = "_";

            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0 || textBox1.Text.Length > 1)
            {
                textBox1.Text = "";
                MessageBox.Show("Unesite jedno slovo!", "Pogreška!");
            }
            else
            {
                if (rijec.Contains(textBox1.Text))
                {
                    for (int i = 0; i < rijec.Length; i++)
                    {
                        if (rijec.IndexOf(textBox1.Text) == rijec.IndexOf(rijec[i]))
                        {
                            switch (i)
                            {
                                case 0:
                                    if (label2.Text == "_")
                                    {
                                        label2.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                                case 1:
                                    if (label3.Text == "_")
                                    {
                                        label3.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                                case 2:
                                    if (label4.Text == "_")
                                    {
                                        label4.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                                case 3:
                                    if (label5.Text == "_")
                                    {
                                        label5.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                                case 4:
                                    if (label6.Text == "_")
                                    {
                                        label6.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                                case 5:
                                    if (label7.Text == "_")
                                    {
                                        label7.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                                case 6:
                                    if (label8.Text == "_")
                                    {
                                        label8.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                                case 7:
                                    if (label9.Text == "_")
                                    {
                                        label9.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                                case 8:
                                    if (label10.Text == "_")
                                    {
                                        label10.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                                case 9:
                                    if (label11.Text == "_")
                                    {
                                        label11.Text = textBox1.Text;
                                        pogodak++;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Slovo je već unešeno!", "Greška!");
                                        break;
                                    }
                                    break;
                            }
                        }
                    }
                    if (pogodak == rijec.Length)
                    {
                        MessageBox.Show("You Won!", "Win!");
                        Application.Exit();
                    }
                }
                else
                {
                    if (zivot == 0)
                    {
                        MessageBox.Show("Game Over", "END");
                        Application.Exit();
                    }
                    else
                    {
                        zivot--;
                        Zivot.Text = "zivoti:" + zivot;
                    }
                }
            }
        }

        private void Zivot_Click(object sender, EventArgs e)
        {

        }
    }


}
